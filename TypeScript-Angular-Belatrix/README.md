
<h1>TypeScript con Angular por Belatrix</h1>

<h3>Alan Buscaglia</h3>

<h1>Tabla de Contenido</h1>

- [1. Fundamentos de TypeScript](#1-fundamentos-de-typescript)
  - [¿Qué es Typescript?](#qué-es-typescript)
  - [Resumen de Typescript](#resumen-de-typescript)
  - [Types, Type Inference y Type keyword](#types-type-inference-y-type-keyword)
  - [Clases](#clases)
  - [Interfaces](#interfaces)
  - [Shapes](#shapes)
  - [Union types](#union-types)
  - [Intersection types](#intersection-types)
  - [Function type](#function-type)
  - [Decorators: aplicación en métodos](#decorators-aplicación-en-métodos)
  - [Decorators: aplicación en clases](#decorators-aplicación-en-clases)
  - [Decorators: aplicación en propiedades](#decorators-aplicación-en-propiedades)
  - [Decorators: aplicación en parámetros](#decorators-aplicación-en-parámetros)
- [2. Preparando el entorno para nuestro proyecto](#2-preparando-el-entorno-para-nuestro-proyecto)
  - [¿Qué es Angular?](#qué-es-angular)
  - [Angular CLI y Visual Studio Code](#angular-cli-y-visual-studio-code)
  - [Creando nuestro proyecto con Angular CLI](#creando-nuestro-proyecto-con-angular-cli)
  - [¿Qué es Firebase? Implementando Firebase en nuestro proyecto](#qué-es-firebase-implementando-firebase-en-nuestro-proyecto)
  - [Diferencias entre Angular, React, Vue](#diferencias-entre-angular-react-vue)
- [3. Desarrollo de la aplicación](#3-desarrollo-de-la-aplicación)
  - [Presentación de la aplicación](#presentación-de-la-aplicación)
  - [Creación de las interfaces](#creación-de-las-interfaces)
  - [Creación e implementación de endpoints en Firebase](#creación-e-implementación-de-endpoints-en-firebase)
  - [Creación de la tabla de jugadores](#creación-de-la-tabla-de-jugadores)
  - [Implementación de nuevo jugador a la tabla 1](#implementación-de-nuevo-jugador-a-la-tabla-1)
  - [Implementación de nuevo jugador a la tabla 2](#implementación-de-nuevo-jugador-a-la-tabla-2)
  - [Debugging de la aplicación](#debugging-de-la-aplicación)
  - [Implementación de edición de un jugador de la tabla](#implementación-de-edición-de-un-jugador-de-la-tabla)
  - [Implementación de eliminación de un jugador en la tabla](#implementación-de-eliminación-de-un-jugador-en-la-tabla)
- [4. Conclusiones](#4-conclusiones)
  - [Conclusiones](#conclusiones)

# 1. Fundamentos de TypeScript

## ¿Qué es Typescript?

**¿Qué es FrontEnd?**
Todo aquel código desarrollado y ejecutado desde el cliente (browser). El browser entiende solamente JS.
…
**¿Qué es BackEnd?**
Todo aquel código que es desarrollado y ejecutado desde el servidor. Posee un mundo sin fin de lenguajes para programar (Python, Java, C, etc).
…
*TypeScript se puede escribir del lado del FrontEnd como BackEnd.*
…
**TypeScript en FrontEnd:** React JS, Angular, Vue JS.
**TypeScript en BackEnd:** Express con Node JS.
…
**Existen dos tipos de lenguajes de programación:**

1. **Lenguaje fuertemente tipado:** dado el valor de una variable de un tipo concreto, no se puede usar como si fuera de otro tipo distinto a menos que se haga una conversión.
2. **Lenguaje no tipado:** tenemos libertad de asignar cualquier tipo valor a nuestras variables.

*JavaScript es un lenguaje no tipado.*
…
Desde el lado del BackEnd es muy frecuente encontrar un lenguaje fuertemente tipado en el cual nosotros vamos a asignar un cierto tipo a nuestras diferentes variables y que no pueden ser reemplazados por algún otro. Por ejemplo: si especificamos que una variable X es de tipo Número, no vamos a poder ingresar algún tipo de valor de tipo String.
…
**¿Qué es Typescript?**
TypeScript es un lenguaje fuertemente tipado creado por Microsoft, aunque está muy relacionado con el framework Angular, TypeScript es un lenguaje independiente que puedes usar para proyectos de BackEnd con Express o FrontEnd ya sea con Vue, React o Angular. Para que TypeScript pueda correr en el navegador debe ser transpilado a JavaScript con herramientas como Babel.
…
**TypeScript sirve para:**

- Brindar un control sobre las aplicaciones

- Cierta libertad para comentar a sus co-workers lo que han querido codificar, esto quiere decir, que TS extiende las funcionalidades de JS Vanilla mediante la inclusion de types dentro de nuestro codigo.

  **Ventajas TS:**

- Nos brinda mayor control al no tener que reutilizar variables mediante diferentes valores

- Dentro de la gran cantidad de IDE’s para programar, encontraremos varias extensiones que nos van a brindar una gran comodidad a la hora de programar. (TSLint, ESLint)

  **Desventajas de TS:**

- JS nos da total libertad para poder incluir cualquier tipo de variable con diferentes valores. Estas variables luego pueden ser reutilizadas sin importar el tipo de esos valores.
  …
  **¿Qué es Lint?**
  Es una herramienta de programación que originalmente era utilizada para detectar errores de programación que escapan al habitual análisis sintáctico que hace el compilador. En la actualidad, se utiliza este término para designar a herramientas que realizan estas tareas de comprobación en cualquier lenguaje de programación.

  **¿Qué es IntelliSense?**
  Es un término general para una variedad de características de edición de código que incluyen: finalización de código, información de parámetros, información rápida y listas de miembros. Las funciones de IntelliSense a veces se llaman con otros nombres como “finalización de código”, “asistencia de contenido” y “sugerencia de código”.

En esta clase el profesor Alan Buscaglia nos explica qué es TypeScript.

TypeScript es un lenguaje fuertemente tipado creado por Microsoft, aunque está muy relacionado con el framework Angular, TypeScript es un lenguaje independiente que puedes usar para proyectos de back-end con Express o Front-end ya sea con Vue, React o Angular. Para que TypeScript pueda correr en el navegador debe ser transpilado a JavaScript con herramientas como Babel.

✔ JavaScript es un lenguaje no tipado, es decir, se tiene la libertad de poder asignar cualquier tipo de valor a nuestras variables.

✔ TypeScript es una librería que extiende la funcionalidad de JavaScript Vanilla mediante la inclusión de types dentro de nuestro código.

![img](https://www.google.com/s2/favicons?domain=https://www.typescriptlang.org//assets/images/icons/favicon-32x32.png)[TypeScript - JavaScript that scales](https://www.typescriptlang.org/)

![img](https://www.google.com/s2/favicons?domain=https://www.typescriptlang.org/docs/home.html/assets/images/icons/favicon-32x32.png)[Documentation · TypeScript](https://www.typescriptlang.org/docs/home.html)

## Resumen de Typescript

En esta infografía te permitirá tener un gran resumen sobre Typescript y sus implementaciones en distintos frameworks de FrontEnd.

<img src="https://i.ibb.co/tQJSGzY/Infografia-Typescript.jpg" alt="Infografia-Typescript" border="0">

**TypeScript** es un lenguaje de programación de código abierto desarrollado por Microsoft, el cual cuenta con herramientas de programación orientada a objetos, muy favorable si se tienen proyectos grandes. Anders Hejlsberg, arquitecto principal del desarrollo del lenguaje de programación C#, es el principal participante en el desarrollo de este lenguaje.

**TypeScript convierte su código en Javascript común**. Es llamado también Superset de Javascript, lo que significa que si el navegador está basado en Javascript, este nunca llegará a saber que el código original fue realizado con TypeScript y ejecutará el Javascript como lenguaje original.

**¿Qué es un superset?**

Se trata de un lenguaje escrito sobre otro lenguaje. En este caso Typescript es eso, un lenguaje basado en el original, ofreciéndonos grandes beneficios como el descrito anteriormente, aunque existen otros beneficios. Por ejemplo, mientras otros superset de JavaScript nos alejan del código original, Typescript, por el contrario, es muy similar a Javascript y a C# gracias a que su creador posee conocimientos de ambos lenguajes.

Actualmente Angular 2, uno de los frameworks más famosos de JavaScript, está siendo desarrollando en TypeScript, para lo cual conocer este lenguaje será fundamental para entender y darle un mejor uso a la nueva versión de Angular.

[TypeScript](https://devcode.la/blog/que-es-typescript/)

**Compilación**

Es el proceso por el cual el código fuente de un programa escrito en un lenguaje de programación “A” se traduce a código fuente de un lenguaje de programación “B”. Cuando el código origen está escrito en un lenguaje de programación de “alto nivel” (C, Java, PHP u otros lenguajes de tercera generación) y se traduce a código máquina que pueda ejecutar directamente el ordenador. Pero hay muchas otras posibilidades como pasar por una representación intermedia en bytecode como es el caso típico de Java.

**Transpilación**

La transpilación es un caso particular de la compilación. Es decir, todo transpilador es también un compilador pero al revés no es cierto, hay compiladores que no se pueden considerar transpiladores. ¿Y cuál es la característica que convierte un compilador en transpilador?: la relación entre los lenguajes origen y destino de la traducción.

Si el compilador traduce código entre dos lenguajes que están al mismo nivel de abstracción entonces, estamos ante un transpilador. Si traduce código entre lenguajes de diferente nivel de abstracción (típicamente de más alto a más bajo nivel) entonces no lo es.

## Types, Type Inference y Type keyword

A diferencia de JavaScript, TypeScript es un lenguaje fuertemente tipado. Mientras que en JavaScript declaramos una constante de la siguiente manera:

```js
const a = 'hola'
```

En TypeScript utilizando el carácter ‘**:**’ le asignamos un tipo de dato a la variable :

```ts
const a: string = 'hola'
```

En caso de que no le asignemos un tipo de dato a la variable, TypeScript **automáticamente** le va a asignar un tipo de dato, esto es el **Type Inference**.

Si le asignamos el tipo de dato **any** a una variable, esta variable va a poder almacenar cualquier tipo de dato, similar a **JavaScript Vainilla**.

Dentro de TypeScript contamos con la palabra reservada **type** que nos va a ser de utilidad cuando hagamos nuestros propios tipos de datos.

**Declaración de Variables en TypeScript**
’let’ y ‘const’ son tipos de declaradóres de variables similares a var, que permiten evitar algunos de los “problemas” de JavaScript.
const, por ejemplo evita la reasignación a una variable.

```tsx
const numLivesForCat = 9; // no se puede re asignar
```

let, impide el block-scoping
Las variables ‘let’ no son visibles fuera de su bloque o bucle for más cercano.

```tsx
let hello = "Hello!"; // declaración normal
// block scoping test
var a = 5;
var b = 10;

if (a === 5) {
  let a = 4; // El alcance es dentro del bloque if
  var b = 1; // El alcance es global
  console.log(a);  // 4
  console.log(b);  // 1
} 
console.log(a); // 5
console.log(b); // 1
```

**Tipos de Datos en TypeScript**
Boolean - El tipo de dato más básico, puede ser true o false

```tsx
let isDone: boolean = false;
```

Number - Los valores numéricos reciben el tipo de dato ‘number’.
También soporta decimal y hexadecimal. Binario y Octal a partir de ECMAScript 2015.

```tsx
let decimal: number = 67;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
```

String - Usamos ‘string’ para referirnos a estos tipos de datos textuales.
Podemos usar comillas dobles (") o simples (’) para rodear la cadena. Tambien los `` para mezclar las cadenas con otras variables.

```tsx
let color: string = "blue";
color = 'red';
let fullName: string = `Bob Bobbington`;
let sentence: string = `Hola!, mi nombre es ${fullName}`;
```

Array - Los tipos de dato Array pueden ser escritos de dos maneras.
1 - Colocamos el tipo de datos seguido de un [], señalamos que sera un array de ese tipo

```tsx
let list: number[] = [1, 2, 3];
```

2 - Usamos un generic, Array<elemType>

```tsx
let list: Array<number> = [1, 2, 3];
```

Any - Cuando aun no sabemos que tipo de dato vamos a utilizar.

```tsx
let notSure: any = 4;
// ...las reasignamos...
notSure = "quizas es un string";
notSure = false; // ahora es un boolean
// si la usamos en un Array podemos mezclar los tipos de datos
let list: any[] = [1, true, "free"];
```

Void - Lo opuesto a any

Visto comunmente en las functions y quiere decir que no retorna ningun valor.

```tsx
function warnUser(): void {
    console.log("Este es mi mensaje");
}
```

## Clases

Dentro de TypeScript podemos realizar programación orientada a objetos y como su nombre lo dice vamos a programar objetos, pero para generar dichos objetos primero debemos crear su plantilla base que recibe el nombre de **clase**.

Una clase está integrada por propiedades y funciones, estas pueden ser privadas, públicas o *protected*.

A la variable que le asignamos el valor de un objeto debemos indicarle que su tipo de dato es igual a la clase.

Una característica de la programación orientada a objetos es que podemos crear clases padre que hereden sus propiedades y funciones a una clase hijo, esto es la **herencia** y dentro de TypeScript se representa con la palabra **extends**.

**Sintaxis General de una clase**

```tsx
class Nombre{
  //atributos
  nivelAcceso nombre: tipoDato;

  //constructor(nombreAtributo: tipoDato){
    this.nombreAtributo =  nombre
  }

  //metodos o comportamientos
  getAtributo(){
   return this.nombreAtributo;
  }

  setAtributo(nombre: tipoDato){
   this.nombreAtributo = nombre;
  }
}
```

Con la palabra reservada extends heredamos una clase a otra.
Con la palabra reservada super accedemos a los atributos y métodos de la clase padre para utilizarlos en la clase hija.

## Interfaces

Una interfaz es un tipo abstracto que sirve como contrato para la **estructura de un objeto** y al igual que las clases puede ser utilizada como un tipo de dato. Para declarar una interfaz en TypeScript utilizamos la palabra clave **interface**.

Dentro de las interfaces en TypeScript podemos manejar **propiedades opcionales** añadiendo el signo de pregunta ‘**?**’ al final del nombre de la propiedad.

<img src="https://i.ibb.co/WP0ZKDh/interface.png" alt="interface" border="0">

También es importante tener en cuenta que a diferencia de las clases, **las interfaces no cuentan con un constructor**, ya que como tal las interfaces no definen a un objeto, sino su comportamiento o estructura.

**Dato curioso:** El nombre **Elvis Operator/Elvis Sign** proviene del famoso cantante estadounidense Elvis Presley. Su peinado se asemeja a un signo de interrogación.

![Elvis Presley](https://i.stack.imgur.com/bVG64.png)

Una interfaz es un contraro que delimita la estructura del objeto a usar. Por ejemplo:

```tsx
interface Materia {
   nombre: string,
   calificacion: number,
   descripcion?: string
}

const materia: Materia = {
	nombre: 'Algebra',
	calificacion: 18,
}
```

En donde la propiedad “descripcion” puede quedad vacía porque es opcional con el símbolo “?”

## Shapes

Cada uno de los tipos tiene una forma o estructura en que es representado, esto es mediante sus propiedades. Dentro de TypeScript si dos variables poseen **propiedades idénticas** aunque sean de **distintas interfaces o clases**, estas pueden ser asignadas entre sí.

> Shapes es una forma de comparar dos clases y saber si tienen las mimas propiedades o si una exíste una relacion hijo-padre entre ellas. Por ejemplo, un pediatra es un médico mas no necesariamente un médico debe ser un pediatra

Personalmente creo que el ejemplo usando herencia de clases no es el mas adecuado para ensenar shapes, bueno al menos no es suficiente, un ejemplo que te hara comprenderlo es este:

```tsx
interface Mamifero{
    cantidad:number
}

interface Comedor{
    comida:string,
    comer():number
}
class Animal implements Mamifero, Comedor{
    comer(): number {
        throw new Error("Method not implemented.");
    }
    cantidad: number;    
    comida: string;


}

class Perro {
    cantidad:number
    comida: string;
    // comer(): number {
    //     throw new Error("Method not implemented.");
    // }
}

let x = new Animal();
let y = new Perro();
x =y; //Error si NO descomentan el metodo comer, es decir si la clase perro no implementa comer
y = x;//Esta asignacion es posible incluso aunque ambos objetos sean instanciados de clases diferentes
```

Este ejemplo muestra que shapes no esta vinculado a la herencia.

Buena observación, sin embargo no hay que perder de vista que el profesor utiliza herencia para poder accesar a las propiedades de la clase *Persona* y esto es dedido a que dichas propiedades son **private**.

De igual manera shapes no está vinculado a implementación de interfaces, más bien Shapes es como moldes creados a partir de las estructuras de las clase (propiedades y métodos), esto permite a Typescript comparar todo el molde o parte de él.

***Shapes\***: son las propiedades que incluye una clase.
El ejemplo que no dio Alan es que una *clase hijo* posee las ***shapes\*** de la *clase padre* por lo tanto:
*clase padre* = *clase hijo* ***no da error\***

**PERO**
La *clase padre* no posee las mismas propiedades de la *clase hijo* (si es que se añadió otra propiedad) por lo tanto:
*clase hijo* = *clase padre* ***si da error\***

## Union types

Habrá ocasiones en las que necesitemos asignar variables con más de un tipo, para evitar el uso de `any` debemos usar **union types**. Utilizando el signo ‘**|**’ podemos indicarle a TypeScript que utilice uno u otro tipo de dato, por ejemplo:

```ts
type SumaParameter = string | number;
```

```tsx
//Habrá ocasiones en las que necesitemos asignar variables con más de un tipo, para evitar el uso de any debemos usar union types. Utilizando el signo ‘|’ podemos indicarle a TypeScript que utilice uno u otro tipo de dato, por ejemplo:

type SumaParameter = string | number;
type SumaReturnType = string | number;

function Suma(num1: SumaParameter, num2: SumaParameter): SumaReturnType {
    return Number(num1) + Number(num2);
}

//Con respecto a las interfaces

interface Interface1 {
    prop1: number;
}

interface Interface2 {
    prop2: number;
}

type InterfaceMix = Interface1 | Interface2;
const interfacemix: InterfaceMix = {
    prop1: 2,
    prop2: 3
}
```

```tsx
//haremos un alias que represente un number y un string
type SumaParameter = string | number;
type SumaReturnType = string | number;

//nuestro parametro podria ser un numero o un string
function Suma(num1: SumaParameter, num2: SumaParameter): SumaReturnType{
    //debemos transformar nuestros valores a number
    //return Number(num1) + Number(num2);
    //Javascript convierte el segundo numero a string porque detecta el primero lo es
    return String(num1)+ num2;
}

//Union types tambien pueden hacerse con interfaces o clases
interface Interface1 {
    prop1: number;
}
interface Interface2 {
    prop2: number;
}

type InterfaceMix = Interface1 | Interface2;

//creamos un objeto de tipo interfacemix
const InterfaceMix: InterfaceMix = {
    prop1: 2,
    prop2: 2,
}
```

Typescript lograría funcionar como Javascript Vanilla con lo que aprendimos hoy ***\**Union Types\*\****

```tsx
let a: string | number = 'hola';
a = 2;
```

## Intersection types

Mientras que el union type nos es útil para variables con más de un tipo de dato, **intersection types** nos permite combinar varios tipos de dato utilizando el signo ‘**&**’.

 Typescript define en forma diferente lo que sería una intersección o unión de tipos en cuanto a lo que conocemos normalmente por la teoría de conjuntos, en Typescript una intersección habla sobre la combinación de los tipos por lo que el resultante tiene todas las propiedades de los mismos. Una unión de tipos es un caso de uno u otro :).

```tsx
//Intersection types y su diferencia de Union types.
interface Interface1 {
    prop1: number;
}
interface Interface2 {
    prop2: number;
    prop3: number; // Si se incluyera ésta propiedad, nuevamente daría error.
}
interface Interface3 {
    prop2: number;
    // prop3: number; Si se incluyera ésta propiedad, nuevamente daría error.
}
type InterfaceMix = Interface1 & Interface2;
const interfaceMix: InterfaceMix = { 
    prop1: 2,
    prop2: 2,
    prop3: 2
}
// La principal diferente entre la union y la intersección es que la unión es un valor "o" el otro, en cambio
// la intersección nos indica que un valor no puede estar indistinto del otro.
// Se puede utilizar en cualquier cantidad de interfaces, agregando si se desea una Interface3 para ver que si
// procede el código.
// Lo importante es que TypeScript nos indica cuando se da éste tipo de error, y así poder corregirlo y llevar
// una mejor gestión del código.
```

Typescript una intersección habla sobre la combinación de los tipos por lo que el resultante tiene todas las propiedades de los mismos. Una unión de tipos es un caso de uno u otro :). 

## Function type

Un *function type* **representa la estructura** que tendrá la función a la cual se aplica el tipo y **únicamente** funciona para tipos de **funciones**.

Los Functions types permiten definir la estructura de nuestra función, como por ejemplo los argumentos y retornos de la mísma.

```tsx
type AreaRectangulo = (altura: number, base: number) => number;

const getAreaRectangulo: AreaRectangulo = (altura: number, base: number ): number => {
    return altura * base;
}
```

[Repositorio del curso](https://github.com/jhon-hst/Curso-de-TypeScript-con-Angular-por-Belatrix)

```tsx
type CallBackError=Error | null;
type CallBack=(error: CallBackError, response: Object) => void;
type sendRequest =(cb: CallBack)=>void;



const sendRequest: sendRequest=(cb: CallBack):void => {
    if (cb) {
        cb(null, { message:'todo salio como lo planeamos'});
    }
}
```

```tsx
//Este alias CallBackError puede ser un error o nulo
type CallBackError = Error | null;
//CallBack recibe como parametro un error de tipo CallBackError y un response de tipo Object
type CallBack = (error: CallBackError, response: Object) => void;
type SendRequest = (callback: CallBack) => void
//void cuando la funcion no retorna nada (callback es del tipo CallBack)
//send request 
const sendRequest: SendRequest = (callback: CallBack): void =>{
    if (callback) {
        callback(null, {message: 'Todo salio bien'});
    }
}
```

> Un function type representa **la estructura que tendrá la función a la cual se aplica el tipo** y únicamente funciona para tipos de funciones.

## Decorators: aplicación en métodos

Los decorators son muy utilizados en Angular, estos son una declaración que tiene TypeScript para poder extender la funcionalidad de distintos elementos ya sea una clase, un parámetro, una propiedad o una función.

En una función decorator, el parámetro **target** hace referencia al objeto que posee el decorador y el parámetro **propertyKey**, o key, hace referencia al elemento que extendemos.

```tsx
function log(target, key) {
  console.log(key + 'se ha llamado');
}

class Persona3 {
  private name: string;

  constructor(name:string) {
    this.name = name;
  }

  @log
  sayMyName() {
    console.log(this.name);
  }
}

const persona3: Persona3 = new Persona3('Alan');
persona3.sayMyName() // 'Alan' // 'sayMyName se ha llamado'
```

> decorators y el `tsconfig.json` tiene que apuntar a ES5, [config](https://repl.it/@tecnocrata/DecoratorsInMethods)
>
> Podemos agreagar al directorio donde estemos trabajando. el archivo `tsconfig.json` y habilitar la opción para decorators
>
> ```json
> {
>     "compilerOptions": {
>         "target": "ES2020",
>         "experimentalDecorators": true
>     }
> }
> ```
>
> Asi el lint que usemos no marcará error al agregar `@<decorator>` en proximos ejercicios.

Los decorators son muy utilizados en Angular, estos son una declaración que tiene TypeScript para poder extender la funcionalidad de distintos elementos ya sea una clase, un parámetro, una propiedad o una función.

En una función decorator, el parámetro target hace referencia al objeto que posee el decorador y el parámetro propertyKey, o key, hace referencia al elemento que extendemos.

## Decorators: aplicación en clases



```tsx
function init(target) {
  return class extends target {
    nombre = 'Alan';
    apellido: 'Buscaglia';

    sayMyName() {
      return `${this.nombre} ${this.apellido}`;
    }
  }
}

@init
class P {
  constructor () {}

  sayMyName() {}
}

const p: P = new P();
console.log(p.sayMyName()); // Alan Buscaglia

// Coment code
function init(target) {
    //retorna la clase usada extendida con el target(parametro)
    return class extends target{
        nombre = 'Luffy';
        apellido = 'Monkey D.';

        sayMyName(){
            return `${this.apellido} ${this.nombre}`;
        }
    }
}

@init
class P{
    constructor(){}

    sayMyName(){}
}
//los decorators extienden la funcionalidad de cierto elemento

const p: P = new P();
console.log(p.sayMyName());
```

> Un decorador es una función que recibe otra función como argumento y regresa la misma función pero “mejorada”

## Decorators: aplicación en propiedades

`Codigo ejecuatable`

```tsx
function logProperty(target:any, key:any) {
  let _val:any = target[key];
  const getter = () => {
    console.log(`Get: ${key} => ${_val}`);
    return _val;
  }
  const setter = (newValue:any) => {
    console.log(`Set: ${newValue} => ${key}`);
    _val = newValue;
  }
  
  const objectProperty = {
    get: getter,
    set: setter
  }
  
  // Sobre-escritura de las propiedades 
  Object.defineProperty(target, key, objectProperty);
}

class Persona {
  @logProperty
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}

const p = new Persona('Alan');
p.name = 'Platzi'; // Set: name => 'Platzi'
const myName = p.name; // Get: name => 'Platzi'
```

> Los decorator es una manera mas limpia y ordenada de escribir código.

## Decorators: aplicación en parámetros

ejemplo ligeramente modificado

```tsx
function logParameter(target:any, propertyName:any, index:any) {
  // identificamos cada parametro que tenemos
  const metadataKey = `log_${propertyName}_parameters`;
  // checkeamos si es o no un arreglo
  if (Array.isArray(target[metadataKey])) {
    // agregamos nuestro index al array
    target[metadataKey].push(index);
  } else {
    // creamos un arreglo con index como único parametro
    target[metadataKey] = [index];
  }
  console.log(target[metadataKey]);
}

class P {
  greet(message: string, @logParameter additional:string): string {
    return message;
  }
}

const p = new P();
p.greet('Hola','Enrique');
```

```tsx
// haremos un log para ver que parametros tenemos en nuestra función
function logParameter(target, propertyName, index) {
  // identificamos cada parametro que tenemos
  const metadataKey = `log_${propertyName}_parameters`;
  // checkeamos si es o no un arreglo
  if (Array.isArray(target[metadatakey])) {
    // agregamos nuestro index al array
    target[metadataKey].push(index);
  } else {
    // creamos un arreglo con index como único parametro
    target[metadataKey] = [index];
  }
}

class P {
  greet(@logParameter message: string): string {
    return message;
  }
}

const p = new P();
p.greet('Hola');
```

# 2. Preparando el entorno para nuestro proyecto

## ¿Qué es Angular?

Desarrollado por Google, **Angular es más que un framework**, es una plataforma que nos da la posibilidad de **desarrollar aplicaciones web** como **aplicaciones mobile**. Además, es un framework de estructura que nos va a brindar funcionalidades para extender el template de nuestra aplicación.

Algunas ventajas que trae Angular son:

- **Rapidez**.
- Mayor **estructura** y **control** del proyecto.
- SPA
- Gran comunidad que ayuda con cualquier problema.

**¿Qué es Angular?**
Mas que un Framework, es una Plataforma.

Desarrollado por Google, Angular es más que un framework, es una plataforma que nos da la posibilidad de desarrollar aplicaciones web como aplicaciones mobile. Además, es un framework de estructura que nos va a brindar funcionalidades para extender el template de nuestra aplicación.

**Algunas ventajas que trae Angular son:**

- Rapidez.

- Mayor estructura y control del proyecto.

- SPA

- Gran comunidad que ayuda con cualquier problema.

  **En detalle:**
  Podemos decir que es un Framework de Estructura. El cual es aquel que nos va a brindar ciertas funcionalidades que podemos agregar a nuestro template para extender y brindar nueva lógica a nuestra app.

  **Posee Directivas, como:**
  ng-if
  ng-if-else
  ng-for

Las cuales dan una nueva funcionalidad a nuestros tags de nuestro html.

Por otro lado, nos brinda un **gran control sobre nuestras aplicaciones**. Además de **gran rapidez y efectividad a la hora de desarrollar nuestras aplicaciones al igual que una estructura solida.**

**Angular way** > ofrece una gran comunidad que nos dice las mejores practicas. Es denominado un ***framework opinado***.

**¿Qué podemos desarrollar con Angular?**

- Progressive Web Apps
- Native Apps
- Desktop Apps.

1. Angular es desarrollado por Google
2. Es más que un framework.
3. Es una plataforma que nos da la posibilidad de desarrollar aplicaciones web como aplicaciones mobile.
4. Es un framework de estructura que nos va a brindar funcionalidades para extender el template de nuestra aplicación.

[![img](https://www.google.com/s2/favicons?domain=https://angular.io/assets/images/favicons/favicon.ico)Angularhttps://angular.io/](https://angular.io/)

## Angular CLI y Visual Studio Code

Angular CLI es la **interfaz de línea de comandos** de Angular con la cual desde una **terminal** puedes crear aplicaciones, generar componentes, montar un servidor local para tu aplicación y testear tu proyecto.

Para este curso y para trabajar con TypeScript o Angular, es muy recomendado utilizar el editor de código [Visual Studio Code](https://code.visualstudio.com/).

Comillas simples y 140

**Extensiones a instalar**
Angular 8 Snippets
Bracket Pair Colorizer
Bracket Pair Colorizer 2
TSLint
ESLint
JavaScript (ES6) code snippets
Prettier - Code formatter

[![img](https://www.google.com/s2/favicons?domain=https://code.visualstudio.com/favicon.ico)Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

[![img](https://www.google.com/s2/favicons?domain=https://cli.angular.io/favicon.ico)Angular CLI](https://cli.angular.io/)

## Creando nuestro proyecto con Angular CLI

Para preparar nuestro entorno de trabajo lo primero que debemos hacer es instalar [Node.js](https://nodejs.org/es/). Una vez tenemos instalado Node en nuestro computador, debemos instalar el CLI de Angular mediante el comando:

```shell
npm install -g @angular/cli
```

Ya que tenemos listo el CLI, creamos nuestro proyecto con el siguiente comando y contestando las preguntas de configuración que nos haga:

```shell
ng new typescript-platzi
```

1. Descargar Nodejs
2. Instalar Angular CLI con npm *en la terminal*

```sh
npm install -g @angular/cli
```

1. Crear un nuevo proyecto con Angular Cli *en la terminal*

```sh
ng new typescript-platzi
```

1. En package.json modificar `“rexjs”` y darle un valor de` “~6.0.0”` 

   ```json
       "rxjs": "6.0.0",
   ```

2. *En la terminal* vamos al directorio de nuestro proyecto

```sh
cd typescript-platzi
```

1. *En la terminal* actualizamos nuestro proyecto porque modificamos el package.json

```sh
npm update
```

1. Vamos a la consola de nuestro navegador, luego damos click a los tres puntos arriba a la derecha, damos click a settings y vamos hasta donde dice **Network** y chuleamos _ _.
2. *En la terminal* cargamos nuestro servidor

```sh
ng serve
```

3. TENEMOS NUESTRA APLICACION!

### Error en Angular

Instalar [nvm](https://github.com/nvm-sh/nvm) y configurar nodejs en la version `14.0.0/ lts/fermium`.

Instalar Angular

Configuracion adicional para la instalacion de [angular | Version](https://www.tutorialspoint.com/angular8/angular8_installation.htm)

```sh
npm install -g @angular/cli@^8.0.0
```

crear proyecto

```sh
ng new typescript-platzi
```

Modificamos el `package.json` `rxjs` en la version 6.0.0

```JSON
"rxjs": "~6.0.0",
```

Actualizar de npm

```sh
npm update
# Error al actualizar
npm update --force
```

En terminal mostrara el siguiente mesaje

```sh
on http://localhost:4200/ 
ℹ ｢wdm｣: Compiled successfully.
```

Proyecto creado satisfactoriamente.

[![img](https://www.google.com/s2/favicons?domain=https://nodejs.org/es//static/favicon.png)Node.js](https://nodejs.org/es/)

[![img](https://www.google.com/s2/favicons?domain=https://cli.angular.io/favicon.ico)Angular CLI](https://cli.angular.io/)

## ¿Qué es Firebase? Implementando Firebase en nuestro proyecto

Firebase es un **SaaS de Google** que nos ayuda en la **creación de aplicaciones web y móvil**. Firebase nos brinda una opción sencilla y rápida para nuestra base de datos y backend.

Dentro de Firebase podemos tener **bases de datos en tiempo real** o *realtime databases*. Podemos usar Firebase independientemente del lenguaje o framework en el que estemos trabajando.

Para añadir Firebase a nuestro proyecto debemos instalar algunas dependencias con el comando:

```shell
# El --save, como se ha comentado, ya no es necesario a partir de la versión 5+ de npm.

npm install firebase @angular/fire

```

NOTA IMPORTANTE:

- El modulo `angularfire2` esta obsoleto, en su lugar escribir el comando: 

  ```sh
  npm i @angular/fire
  ```

  

- Si cuando compilas con `“ng serve”` tienes problemas actualiza a la ultima version de Angular V9 (5/2020): 

  ```sh
  ng update @angular/cli @angular/core
  ```

  Importar en `app.module.ts`

  ```js
  import { AngularFireModule } from '@angular/fire';
  import { AngularFireDatabaseModule } from '@angular/fire/database';
  
  // Options 2, run server
  import { AngularFireModule} from '@angular/fire/compat'
  import { AngularFireDatabaseModule } from '@angular/fire/compat/database'
  ```
  
  [![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/vf2c4ef0d44527f056856a39978a8abe9c314ecfe03120ff2c73cbf0ecdfbe4b1/firebase/images/favicon.png)Firebase](https://firebase.google.com/)

## Diferencias entre Angular, React, Vue

Cuando vamos iniciando en el mundo del front-end o simplemente estamos creando un nuevo proyecto nos encontraremos con una importante pregunta:

### ¿Qué framework debo utilizar?

Lo primero que te llega a la mente va a ser los tres frameworks más populares:

#### Angular

Si tienes un proyecto complejo y robusto Angular es tu mejor opción ya que al estar pensado en trabajar con TypeScript ofrece una gran robustez, estructura y control.

Angular CLI es el CLI más completo para trabajar.

Desventajas:

- Al ser un framework tan robusto su curva de aprendizaje es muy elevada y compleja.
- Tendremos código repetitivo que genera archivos muy grandes.

#### React

Ventajas:

- Creado por Facebook, ofrece una gran flexibilidad para trabajar basado en componentes.
- Cuenta con una gran comunidad, por lo tanto muchos problemas con los que te encuentres ya habrán sido resueltos por alguien.

Desventajas:

- Hay muchas formas de resolver un mismo problema, por lo tanto hay miles de librerías y tal vez pocas sean la solución correcta.

#### Vue

También está basado en componentes, cuenta con una gran usabilidad y una curva de aprendizaje muy fácil.

Su mayor desventaja es que al ser muy nuevo, su comunidad es muy nueva y es probable que los problemas con los que te encuentres tendrás que crear tu propia solución.

Ningún framework es mejor que el otro, cada uno cumple una funcionalidad distinta y cuenta con sus propias ventajas y desventajas.

De los 3 frameworks, es **Angular** el mas nuevo, sin embargo creo que el profesor se referia a **AngularJS**, el cual si es mas antiguo que los otros 3. Angular es la evolución de AngularJS aunque incompatible con su predecesor.

**Fechas de lanzamiento:**

- **Angular**: 14 de septiembre de 2016

- **Vue**: Febrero de 2014

- **React**: 29 de mayo de 2013

- **AngularJS**: 20 de octubre de 2010

  El profesor debe referirse a que Vue es un baby debido a que es el que menos comunidad ha tenido, y creo que esto se debe en gran parte porque **tanto Angular como React fueron creados por empresas grandes** (**Google** y **Facebook** respectivamente) **sin embargo, Vue no.**

  **Evan You fue el creador de Vue**, después de trabajar para Google usando AngularJS en varios proyectos. Más tarde resumió su proceso de pensamiento: “*Pensé, ¿qué pasaría si pudiera extraer la parte que realmente me gustó de Angular y construir algo realmente liviano*”. **Es por ello que Vue y Angular tienen algo de similaridad.**

# 3. Desarrollo de la aplicación

## Presentación de la aplicación

CRUD = Create Read Update Delete.

## Creación de las interfaces

### Enums

Las enumeraciones nos permiten definir un conjunto de constantes con nombre. El uso de enumeraciones puede facilitar la documentación de la intención o crear un conjunto de casos distintos. TypeScript proporciona enumeraciones numéricas y basadas en cadenas.

- **Enumeraciones numéricas**

Primero comenzaremos con enumeraciones numéricas, que probablemente sean más familiares si viene de otros idiomas. Se puede definir una enumeración usando la `enum` palabra clave.

```tsx
enum Direction {
    Up = 1,
    Down,
    Left,
    Right,
}
```

Arriba, tenemos una enumeración numérica donde `Up` se inicializa con 1. Todos los siguientes miembros se incrementan automáticamente a partir de ese punto. En otras palabras, `Direction.Up` tiene el valor 1, `Down`tiene 2, `Left` tiene 3 y `Right` tiene 4.

Si quisiéramos, podríamos dejar los inicializadores por completo:

```tsx
enum Direction {
    Up,
    Down,
    Left,
    Right,
}
```

Aquí, `Up` tendría el valor 0, `Down` tendría 1, etc. Este comportamiento de incremento automático es útil para los casos en los que no nos interesen los valores de los miembros, pero sí nos importa que cada valor sea distinto de otros valores en la misma enumeración.

> Las buenas practicas dicen que se debe crear un archivo por cada modelo o enum, ademas si es un modelo el nombre debería ser: [nombre].model.ts o [nombre].enum.ts y no todo en un solo archivo.

También se pueden utilizar las instrucciones:

```sh
ng g interface interfaces/player
ng g interface interfaces/team
```

y las variables `enum` se dejan en una carpeta llamada `./globals` y se importan a las `interfaces`.

**Countries de un Gist**. Pero, **qué es un Gist?**
.
**Gist es un método sencillo para compartir fragmentos o extractos de datos con otros. Una esencia puede ser una cadena de código, un script bash o algún otro dato pequeño.**

GitHub aloja estos bits de información como un repositorio.
No solo los gists son gratuitos para crear y almacenar en GitHub, sino que también son fáciles de crear.

> Tip: Para tener una buena estructura de nuestro proyecto es recomendable crear una interfaz por cada entidad que tenga el proyecto!

[Cómo crear un Gist en GitHub?](https://docs.github.com/es/github/writing-on-github/creating-gists)

[TypeScript enum Country Codes ISO 3166](https://gist.github.com/kyranjamie/646386d5edc174e8b549111572897f81)

[kyranjamie | countries.enum.ts](https://gist.github.com/kyranjamie/646386d5edc174e8b549111572897f81?permalink_comment_id=4089368)

## Creación e implementación de endpoints en Firebase

se crean los servicies desde la terminal ,por ejemplo para que cree la carpeta services y lo ponga dentro

```sh
ng generate service services/-nombre del servicio-
```

o mejor aun, de forma simplificada

```sh
ng g s services/-nombre del servicio-
```

Player Service:

```tsx
getPlayers(): Observable<Player[]> {
    return this.playerDb.snapshotChanges().pipe(
      map((changes) => {
        return changes.map(
          (c) =>
            ({
              $key: c.payload.key,
              ...c.payload.val(),
            } as Player)
        );
      })
    );
  }
```

Team Service:

```tsx
getTeams(): Observable<Team[]> {
    return this.teamDb.snapshotChanges().pipe(
      map((changes) => {
        return changes.map(
          (c) =>
            ({
              $key: c.payload.key,
              ...c.payload.val(),
            } as Team)
        );
      })
    );
  }
```

---

team.service.ts

```tsx
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Team } from '../interfaces/team';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export const TeamTableHeaders = ['name', 'country', 'players'];

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teamsDB: AngularFireList<Team>;

  constructor(private db: AngularFireDatabase) {
    this.teamsDB = this.db.list('/teams', ref => ref.orderByChild('name'));
  }

  getPlayers(): Observable<Team[]> {
    return this.teamsDB.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({ $key: c.payload.key, ...c.payload.val() }));
      })
    );
  }
  addTeam(team: Team) {
    return this.teamsDB.push(team);
  }

  deleteTeam(id: string) {
    this.db.list('/teams').remove(id);
  }

  editTeam(newPlayerData) {
    const $key = newPlayerData.$key;
    delete newPlayerData.$key;
    this.db.list('/teams').update($key, newPlayerData);
  }
}
```

player.service.ts

```tsx
import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Player } from '../interfaces/player';
import { Observable, observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private playersDB: AngularFireList<Player>;

  constructor(private db: AngularFireDatabase) {
    this.playersDB = this.db.list('/players', ref => ref.orderByChild('name'));
  }
  getPlayers(): Observable<Player[]> {
    return this.playersDB.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({ $key: c.payload.key, ...c.payload.val() }));
      })
    );
  }

  addPlayer(player: Player) {
    return this.playersDB.push(player);
  }

  deletePlayer(id: string) {
    this.db.list('/players').remove(id);
  }

  editPlayer(newPlayerData) {
    const $key = newPlayerData.$key;
    delete newPlayerData.$key;
    this.db.list('/players').update($key, newPlayerData);
  }
}
```

Y no olvidar agregar los servicios al app.module.ts

```tsx
providers: [PlayerService,TeamService],
```

## Creación de la tabla de jugadores

`css`

```scss
body {
  background: -webkit-linear-gradient(left, #25c481, #25b7c4);
  background: linear-gradient(to right, #25c481, #25b7c4);
}
section {
  margin: 50px;
}

h1 {
  font-size: 30px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 300;
  text-align: center;
  margin-bottom: 15px;
}
table {
  width: 100%;
  table-layout: fixed;
}
.tbl-header {
  background-color: rgba(255, 255, 255, 0.3);
}
.tbl-content {
  overflow-x: auto;
  margin-top: 0px;
  border: 1px solid rgba(255, 255, 255, 0.3);
}

.tbl-content-player {
  overflow-x: auto;
  margin-top: 0px;
  border: 1px solid rgba(255, 255, 255, 0.3);
  height: 300px;
}

th {
  padding: 20px 15px;
  text-align: left;
  font-weight: 500;
  font-size: 12px;
  color: #fff;
  text-transform: uppercase;
  border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}
td {
  padding: 15px;
  text-align: left;
  vertical-align: middle;
  font-weight: 300;
  font-size: 12px;
  color: #fff;
  border-bottom: solid 1px rgba(255, 255, 255, 0.1);
}

.modal-window {
  position: fixed;
  background-color: rgba(255, 255, 255, 0.25);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
  &:target {
    opacity: 1;
    pointer-events: auto;
  }
  & > div {
    width: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 2em;
    background: #ffffff;
    color: #333333;
  }
  header {
    font-weight: bold;
  }
  h1 {
    font-size: 150%;
    margin: 0 0 15px;
    color: #333333;
  }
}

.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
  &:hover {
    color: #000;
  }
}

form {
  .form-group {
    padding-bottom: 15px;
  }

  input,
  textarea,
  select {
    width: 235px;
    background-color: #f6f6f6;
    border: solid 1px #25c481;
    font-size: 13px;
    color: #000000;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 5px;
    padding-right: 5px;
  }

  select {
    width: 246px;
  }

  label {
    font-size: 14px;
    color: #555555;
    font-weight: normal;
    display: block;
    float: left;
    margin: 0;
    padding: 3px 13px 0 0;
    text-align: right;
    width: 140px;
  }

  .checkbox {
    width: auto;
  }

  .submit-container {
    text-align: right;
  }

  .submit {
    background-color: #25b7c4;
    border: solid 1px #25c481;
    font-size: 16px;
    color: #ffffff;
    font-weight: bold;
    padding-top: 8px;
    padding-bottom: 8px;
    padding-right: 25px;
    padding-left: 25px;
    -moz-border-radius: 15px;
    -webkit-border-radius: 15px;
    border-radius: 15px;
    margin-top: 0px;
    margin-bottom: 0px;
  }
  &.required label {
    font-weight: bold;
  }

  span.required label {
    font-weight: bold;
  }
}

/* Demo Styles */

html,
body {
  height: 100%;
}

body {
  font: 600 18px/1.5 -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
  background-color: #f69d75;
  color: #555555;
}

a {
  color: inherit;
}

.container {
  justify-content: center;
  align-items: center;
  height: 50px;
}

.btn {
  background-color: #fff;
  padding: 1em 1.5em;
  border-radius: 3px;
  text-decoration: none;
  i {
    padding-right: 0.3em;
  }
}

::-webkit-scrollbar {
  width: 6px;
}
::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
}
::-webkit-scrollbar-thumb {
  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
}
```

codigo que  va en el archivo `…src/styles.scss` de tu proyecto.

código:

```tsx
import { Component, OnInit } from '@angular/core';
import { TeamService, TeamsTableHeaders } from '../services/team.service';
import { Observable } from 'rxjs';
import { take} from 'rxjs/operators';
import { Team } from '../interfaces/team';
import { Countries } from '../interfaces/player';

@Component({
  selector: 'app-team-table',
  templateUrl: './team-table.component.html',
  styleUrls: ['./team-table.component.scss'],
})
export class TeamTableComponent implements OnInit {
  public teams$: any = new Observable<Team[]>();
  //public teams$: Observable<Team[]> | undefined;
  public tableHeaders = TeamsTableHeaders;

  constructor(private teamService: TeamService) {}

  ngOnInit() {
    this.teams$ = this.teamService.getTeams();
    this.teamService
      .getTeams()
      .pipe(take(1))
      .subscribe(teams => {
        if (teams.length === 0) {
          const team: Team = {
            name: 'MyAmazingTeam',
            country: Countries.Argentina,
            players: null!,
          };
          this.teamService.addTeam(team);
        }
      });
  }
}
```

`team-table-service.html`

```html
<section>
  <h1>My Team</h1>
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th *ngFor="let header of tableHeaders">{{header}}</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <tr *ngFor="let team of (teams$ | async)">
          <th>{{team.name}}</th>
          <th>{{team.country}}</th>
          <th><p *ngFor="let player of team.player">
            {{player.name}}
          </p></th>
        </tr>
      </tbody>
    </table>
  </div>
</section>
```

[Repositorio](https://github.com/AppleLAN/typescript-platzi)

## Implementación de nuevo jugador a la tabla 1

Para traer los headers de la tabla de jugadores, lo podemos hacer de forma similar a los headers de la tabla de equipos, así:

- En player.service.ts agregamos:

```tsx
export const PlayersTableHeaders = ['Name', 'Last Name', 'Position', 'Weight', 'Height', 'Nationality', 'Left Footed', 'Actions'];
```

- En player-table.component.ts agregamos:

```tsx
public playersTableHeaders = PlayersTableHeaders;
```

- En player-table.component.html agregamos:

```html
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th *ngFor="let header of playersTableHeaders">{{ header }}</th>
        </tr>
      </thead>
    </table>
```

`player-dialog.components.ts`

```tsx
import { Component, OnInit } from '@angular/core';
import { Country, Player, SquadNumber } from '../interfaces/player';
import { Team } from '../interfaces/team';

import { take } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { PlayerService } from '../services/player.service';
import { TeamService } from '../services/team.service';

declare type CategoryType = keyof typeof Country;
@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss'],
})
export class PlayerDialogComponent implements OnInit {
  private team!: Team;
  
  // public countries = Object.keys(Country).map((key) => ({
  //   label: key,
  //   key: Country[key as CategoryType],
  // }));
  public countries = Object.keys(Country).map((key) => ({
    label: key,
    key: key.indexOf
    // key: (<any>Country)[key]
  }));

  public squadNumber = Object.keys(SquadNumber)
    .slice(Object.keys(SquadNumber).length / 2)
    .map((key: any) => ({
      label: key,
      key: SquadNumber[key],
    }));
  constructor(
    private playerService: PlayerService,
    private teamService: TeamService
  ) {}

  ngOnInit(): void {
    this.teamService
      .getTeams()
      .pipe(take(1))
      .subscribe((teams) => {
        if (teams.length > 0) {
          this.team = teams[0];
        }
      });
  }

  private newPlayer(playerFormValue: Player) {
    const key = this.playerService.addPlayer(playerFormValue).key;
    const playerFormValueKey = {
      ...playerFormValue,
      key,
    };
    const formattedTeam = {
      ...this.team,
      players: [
        ...(this.team.players ? this.team.players : []),
        playerFormValueKey,
      ],
    };
    this.teamService.editTeam(formattedTeam);
  }
}
```

## Implementación de nuevo jugador a la tabla 2

```tsx
...
export class PlayerDialogComponent implements OnInit {
  private team!: Team;
  public player!: Player;
  public countries = Object.keys(Country).map((key) => ({
    label: key,
    key: key.indexOf,
    // key: (<any>Country)[key]
  }));
...
```

## Debugging de la aplicación

`player-dialog.component.html`

```html
<div id="open-modal" class="modal-window">
  <div>
    <a href="#" title="Close" class="modal-close">Close</a>
    <h1>New Player Form</h1>
    <div>
      <form #playerForm="ngForm" novalidate (ngSubmit)="onSubmit(playerForm)">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" [ngModel]="player?.name" #name="ngModel" ngModel required />
        </div>

        <div class="form-group">
          <label for="lastName">Last Name</label>
          <input type="text" class="form-control" name="lastName" [ngModel]="player?.lastName" #name="ngModel" ngModel required />
        </div>

        <div class="form-group">
          <label for="position">Position</label>
          <select class="form-control" name="position" [ngModel]="player?.position" #name="ngModel" ngModel required>
            <option *ngFor="let position of squadNumber" [value]="position.key">{{ position.label }}</option>
          </select>
        </div>

        <div class="form-group">
          <label for="weight">Weight</label>
          <input type="text" class="form-control" name="weight" [ngModel]="player?.weight" #name="ngModel" ngModel required />
        </div>

        <div class="form-group">
          <label for="height">Height</label>
          <input type="text" class="form-control" name="height" [ngModel]="player?.height" #name="ngModel" ngModel required />
        </div>

        <div class="form-group">
          <label for="nationality">Nationality</label>
          <select class="form-control" name="nationality" [ngModel]="player?.nationality" #name="ngModel" ngModel required>
            <option *ngFor="let nationality of countries" [value]="nationality.key">{{ nationality.label }}</option>
          </select>
        </div>

        <div class="form-group">
          <label for="leftFooted">Left Footed</label>
          <input type="text" class="form-control" name="leftFooted" [ngModel]="player?.leftFooted" #name="ngModel" ngModel required />
        </div>

        <div class="submit-container">
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
```

`player-dialog.component.ts`

```tsx
...
  onSubmit(playerForm: NgForm) {
    const playerFormValue = { ...playerForm.value };
    if (playerForm.valid) {
      playerFormValue.leftFooted =
        playerFormValue.leftFooted === '' ? false : playerFormValue.leftFooted;
    }
    this.newPlayer(playerForm.value);
    window.location.replace('#')
  }
}
```

## Implementación de edición de un jugador de la tabla

**Inmutabilidad de objetos**

La inmutabilidad es la propiedad que tiene un objeto que no puede cambiar su estado. Como consecuencia, la inmutabilidad aporta muchas facilidades a la hora de razonar sobre nuestro código, ya que nos libera de pensar en los cambios sufridos por objetos a lo largo del programa. Además, los objetos inmutables son automáticamente seguros en hilos (o thread-safe), ya que pueden ser accedidos de manera concurrente sin consecuencias, al no poder modificarse.

En programación orientada a objetos generalmente asumimos que el estado de un objeto puede cambiar según los métodos que se llamen sobre el mismo. Sin embargo, en ocasiones podemos construir clases que den lugar a objetos inmutables.

Referencia: https://www.campusmvp.es/recursos/post/programacion-funcional-inmutabilidad-y-funciones-puras.aspx

## Implementación de eliminación de un jugador en la tabla

`player-dialog.component.ts`

```tsx
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Country, Player, SquadNumber } from '../interfaces/player';
import { Team } from '../interfaces/team';

import { take } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { PlayerService } from '../services/player.service';
import { TeamService } from '../services/team.service';

declare type CategoryType = keyof typeof Country;
@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss'],
})
export class PlayerDialogComponent implements OnInit {
  @Input() player: Player;
  @Output() closeDialog: EventEmitter<boolean> = new EventEmitter();
  private team!: Team;
  // public player!: Player;
  public countries = Object.keys(Country).map((key) => ({
    label: key,
    key: key.indexOf,
    // key: (<any>Country)[key]
  }));

  public squadNumber = Object.keys(SquadNumber)
    .slice(Object.keys(SquadNumber).length / 2)
    .map((key: any) => ({
      label: key,
      key: SquadNumber[key],
    }));
  constructor(
    private playerService: PlayerService,
    private teamService: TeamService
  ) {}

  ngOnInit(): void {
    this.teamService
      .getTeams()
      .pipe(take(1))
      .subscribe((teams) => {
        if (teams.length > 0) {
          this.team = teams[0];
        }
      });
  }

  private newPlayer(playerFormValue: Player) {
    const key = this.playerService.addPlayer(playerFormValue).key;
    const playerFormValueKey = {
      ...playerFormValue,
      key,
    };
    const formattedTeam = {
      ...this.team,
      players: [
        ...(this.team.players ? this.team.players : []),
        playerFormValueKey,
      ],
    };
    this.teamService.editTeam(formattedTeam);
  }

  // private editPlayer(playerFormValue: Player) {
  //   // const playerFormValueWithKey = {...playerFormValue, $key: this.player.$key}
  //   const playerFormValueKey = {
  //     ...playerFormValue,
  //     $key: this.player != null ? this.player.$key : null,
  //   };
  //   const playerFormValueWithFormattedKey = {...playerFormValue, key: this.closeDialog.$key };
  //   delete playerFormValueWithFormattedKey.$key;
  // }
  private editPlayer(playerFormValue: Player) {
    const playerFormValueWithKey = {
      ...playerFormValue,
      $key: this.player.$key,
    };
    const playerFormValueWithFormattedKey = {
      ...playerFormValue,
      key: this.player.$key,
    };
    delete playerFormValueWithFormattedKey.$key;
    
    const moddifiedPlayers = this.team.players
      ? this.team.players.map((player: any) => {
          return player.key === this.player.$key
            ? playerFormValueWithFormattedKey
            : player;
        })
      : this.team.players;
    const formattedTeam = {
      ...this.team,
      players: [
        ...(moddifiedPlayers
          ? moddifiedPlayers
          : [playerFormValueWithFormattedKey]),
      ],
    };
    this.playerService.editPlayer(playerFormValueWithKey);
    this.teamService.editTeam(formattedTeam);
  }

  onSubmit(playerForm: NgForm) {
    const playerFormValue = { ...playerForm.value };
    if (playerForm.valid) {
      playerFormValue.leftFooted =
        playerFormValue.leftFooted === '' ? false : playerFormValue.leftFooted;
    }
    if (this.player) {
      this.editPlayer(playerFormValue);
    } else {
      this.newPlayer(playerFormValue);
    }
    // this.newPlayer(playerForm.value);
    window.location.replace('#');
  }

  onClose() {
    this.closeDialog.emit(true);
  }
}
```



# 4. Conclusiones

## Conclusiones

- Qué es TypeScript
- Fundamentos de TypeScript
- Shapes
- Decorators
- Crear un proyecto en Angular

No tengas miedo de utilizar TypeScript en tu próximo proyecto, recuerda tomar el examen del curso y si tienes alguna duda déjala en la sección de discusiones.

Sabemos que fue un largo camino pero estamos más que seguros que valió la pena y **nunca vas a parar de aprender**.
