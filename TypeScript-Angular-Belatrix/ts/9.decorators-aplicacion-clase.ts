

function init(target) {
  return class extends target {
    nombre = 'Alan';
    apellido = 'Buscaglia';

    sayMyName() {
      return `${this.nombre} ${this.apellido}`;
    }
  }
}

@init
class P {
  constructor() {}

  syMyName() {}
}

const p: P = new P();

p.syMyName();
console.log(p.syMyName());    // Alan Buscaglia