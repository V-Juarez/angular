//Intersection types y su diferencia de Union types.
interface Interface1 {
    prop1: number;
}
interface Interface2 {
    prop2: number;
    prop3: number; // Si se incluyera ésta propiedad, nuevamente daría error.
}
interface Interface3 {
    prop2: number;
    // prop3: number; Si se incluyera ésta propiedad, nuevamente daría error.
}
type InterfaceMix = Interface1 & Interface2;
const interfaceMix: InterfaceMix = { 
    prop1: 2,
    prop2: 2,
    prop3: 2
}