
type callBackError = Error | null;
type callBack = (error: callBackError, reponse: Object) => void;
type SendRequest = (cb: callBack) => void;

// function sendRequest(cb: callBack): void {
//   if (cb) {
//     cb(null, {message: 'Todo salio como lo planeamos'});
//   }
// }
const sendRequest: SendRequest = (cb: callBack): void => {
  if (cb) {
    cb(null, {message: 'Todo salio como lo planeamos'});
  }
}