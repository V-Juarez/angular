function logProperty(target:any, key:any) {
  let _val:any = target[key];
  const getter = () => {
    console.log(`Get: ${key} => ${_val}`);
    return _val;
  }
  const setter = (newValue:any) => {
    console.log(`Set: ${newValue} => ${key}`);
    _val = newValue;
  }
  
  const objectProperty = {
    get: getter,
    set: setter
  }
  
  // Sobre-escritura de las propiedades 
  Object.defineProperty(target, key, objectProperty);
}

class Persona {
  @logProperty
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}

const p = new Persona('Alan');
p.name = 'Platzi'; // Set: name => 'Platzi'
const myName = p.name; // Get: name => 'Platzi'