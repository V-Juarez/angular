// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBMIJGWeMnC9m11mDIbMJvpvblUD7t4Ivk",
    authDomain: "angular-platzi-b00f2.firebaseapp.com",
    databaseURL: "https://angular-platzi-b00f2-default-rtdb.firebaseio.com",
    projectId: "angular-platzi-b00f2",
    storageBucket: "angular-platzi-b00f2.appspot.com",
    messagingSenderId: "473160421444",
    appId: "1:473160421444:web:be5e43668c8846e2466c11",
    measurementId: "G-BCT8F54CPG"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
