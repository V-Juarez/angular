<h1>Angular</h1>

<h3>Nicolas Molina</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción a Angular](#1-introducción-a-angular)
  - [Todo lo que aprenderás sobre Angular](#todo-lo-que-aprenderás-sobre-angular)
  - [Instalación y preparación del entorno de desarrollo](#instalación-y-preparación-del-entorno-de-desarrollo)
  - [Preparando el entorno para Windows](#preparando-el-entorno-para-windows)
  - [Preparando el entorno para Linux](#preparando-el-entorno-para-linux)
  - [Crea tu primera aplicación en Angular](#crea-tu-primera-aplicación-en-angular)
- [2. Estructuras de control](#2-estructuras-de-control)
  - [Introducción al Angular CLI y proyecto del curso](#introducción-al-angular-cli-y-proyecto-del-curso)
  - [String interpolation](#string-interpolation)
  - [Data binding en Angular](#data-binding-en-angular)
  - [Uso de ngIf](#uso-de-ngif)
  - [Uso de ngFor add y delete](#uso-de-ngfor-add-y-delete)
  - [Uso de ngFor para recorrer objetos](#uso-de-ngfor-para-recorrer-objetos)
  - [Uso de ngSwitch](#uso-de-ngswitch)
- [3. Componentes](#3-componentes)
  - [¿Qué son los componentes y decoradores?](#qué-son-los-componentes-y-decoradores)
  - [Uso de Inputs y Outputs](#uso-de-inputs-y-outputs)
  - [Ciclo de vida de los componentes](#ciclo-de-vida-de-los-componentes)
  - [Estilos para mostrar la lista de productos](#estilos-para-mostrar-la-lista-de-productos)
  - [Uso de ng generate y ng lint](#uso-de-ng-generate-y-ng-lint)
- [4. Pipes y Directivas](#4-pipes-y-directivas)
  - [Usando los pipes de Angular](#usando-los-pipes-de-angular)
  - [Construyendo un propio pipe](#construyendo-un-propio-pipe)
  - [Construyendo una directiva propia](#construyendo-una-directiva-propia)
- [5. Módulos y Rutas](#5-módulos-y-rutas)
  - [Introducción al NgModule](#introducción-al-ngmodule)
  - [Creando rutas en Angular](#creando-rutas-en-angular)
  - [Creando la página home de la tienda](#creando-la-página-home-de-la-tienda)
  - [Usando routerLink y routerActive](#usando-routerlink-y-routeractive)
  - [Creando el detalle de cada producto](#creando-el-detalle-de-cada-producto)
  - [Elaboración de la página de detalle de producto](#elaboración-de-la-página-de-detalle-de-producto)
  - [Creando el módulo del website con vistas anidadas](#creando-el-módulo-del-website-con-vistas-anidadas)
  - [Preparar el proyecto para Lazy Loading](#preparar-el-proyecto-para-lazy-loading)
  - [Implementación del Lazy Loading](#implementación-del-lazy-loading)
  - [Creando un shared module y core module](#creando-un-shared-module-y-core-module)
  - [Guardianes](#guardianes)
- [6. UI/UX, estilos](#6-uiux-estilos)
  - [alando Angular Material](#alando-angular-material)
  - [](#)
  - [Instalando un sistema de grillas](#instalando-un-sistema-de-grillas)
  - [Creando el header](#creando-el-header)
  - [Estilos a product-card](#estilos-a-product-card)
  - [Creando vistas con Angular schematic](#creando-vistas-con-angular-schematic)
- [7.Servicios](#7servicios)
  - [Creando nuestros propios servicios: HTTP Client](#creando-nuestros-propios-servicios-http-client)
  - [Haciendo una solicitud GET desde el servicio](#haciendo-una-solicitud-get-desde-el-servicio)
  - [Haciendo una solicitud POST desde el servicio](#haciendo-una-solicitud-post-desde-el-servicio)
  - [Haciendo una solicitud PUT y DELETE desde el servicio](#haciendo-una-solicitud-put-y-delete-desde-el-servicio)
  - [Ambientes en Angular](#ambientes-en-angular)
  - [Lista de inventario y detalle](#lista-de-inventario-y-detalle)
  - [Cierre de ejercicio de detalle](#cierre-de-ejercicio-de-detalle)
- [8. Formularios](#8-formularios)
  - [Introducción al FormControl](#introducción-al-formcontrol)
  - [Creando el formulario de productos](#creando-el-formulario-de-productos)
  - [Ajustar estilos en un formulario](#ajustar-estilos-en-un-formulario)
  - [Validaciones personalizadas](#validaciones-personalizadas)
  - [Editar un producto a través de un formulario](#editar-un-producto-a-través-de-un-formulario)
- [9. Programación reactiva](#9-programación-reactiva)
  - [Añadiendo productos al carrito](#añadiendo-productos-al-carrito)
  - [Usa un pipe para hacer un contador de productos](#usa-un-pipe-para-hacer-un-contador-de-productos)
  - [Creando la página de la orden y uso de async](#creando-la-página-de-la-orden-y-uso-de-async)
- [10. Firebase](#10-firebase)
  - [Instalar Angular Firebase y configurar Firebase Auth](#instalar-angular-firebase-y-configurar-firebase-auth)
  - [Implementando Auth y Guards](#implementando-auth-y-guards)
  - [Subiendo una imagen a Firebase Storage](#subiendo-una-imagen-a-firebase-storage)
  - [Haciendo deploy a Firebase Hosting](#haciendo-deploy-a-firebase-hosting)
- [11. Conclusiones](#11-conclusiones)
  - [Repaso del curso](#repaso-del-curso)
# 1. Introducción a Angular

## Todo lo que aprenderás sobre Angular

OBJETIVO
Crear aplicaciones con buenas prácticas, conociendo cada uno de los artefactos, herramientas y el ecosistema que ofrece Angular, al final poder crear aplicaciones de gran calidad para poner a producción.
💪🏼

## Instalación y preparación del entorno de desarrollo

Instalar un Editor de Texto o IDE tal como Visual Studio Code, Sublime Text, Atom, etc… Posteriormente instalar complementos como pluggins y software escencial como Node.js y la librería de Angular en el sistema operativo del equipo.

**Visual Studio Code**

1. Descargar el software de la página oficial: [https://code.visualstudio.com](https://code.visualstudio.com/)
2. En la sección de extensiones instalar:
   [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
   [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

*Nota: Estas extensiones permiten mantener el código limpio, estructurado y actualizado a las reglas del “nuevo” JS.*

**Node.js**

1. Descargar e instalar el Instalador de Node.js: https://nodejs.org/en/download/
2. Abrir una terminal/consola y ingresar el comando:

```bash
node -v
```

Nota: Este comando permite saber la versión de Node.js en nuestro sistema operativo (mediante **-v** se conoce la versión)


**Angular CLI**

Ya teniendo instalado Node.js, podemos instalar en nuestro sistema operativo la libreria de Angular (CLI).

1. Abrimos una consola o terminal.
2. Ingresar el siguiente comando y esperar su instalación-

```bash
npm install -g @angular/cli
```

## Preparando el entorno para Windows

Instalar un Editor de Texto o IDE tal como Visual Studio Code, Sublime Text, Atom, etc… Posteriormente instalar complementos como pluggins y software escencial como Node.js y la librería de Angular en el sistema operativo del equipo.


**Visual Studio Code**:

Pasos:

1. Descargar el software de la página oficial: [https://code.visualstudio.com](https://code.visualstudio.com/)
2. En la sección de extensiones instalar: [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template) y [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

***Nota:** Estas extensiones permiten mantener el código limpio, estructurado y actualizado a las reglas del “nuevo” JS.*


**Node.js**

Pasos:

1. Descargar e instalar el Instalador de [Node.js:](https://nodejs.org/en/download/)
2. Abrir una terminal/consola y ingresar el comando: `node-v`

***Nota:** Este comando permite saber la versión de Node.js en nuestro sistema operativo (mediante -v se conoce la versión)*


**Angular CLI**

Ya teniendo instalado Node.js, podemos instalar en nuestro sistema operativo la libreria de Angular (CLI).

Pasos:

1. Abrimos una consola o terminal.
2. Ingresar el siguiente comando y esperar su instalación:

```bash
npm install -g @angular/cli
```

> Angular es un framework y que trabaja con Typescript, y Typescript se traduce a un código en Javascript. La versión de Angular es básicamente la versión del framework. Node JS es la plataforma sobre la cual se ejecuta el código, a mí forma de verlo es básicamente la plataforma encargada de compilar y correr el código. La versión de Node JS es la versión del en inglés runtime environment, que Seria el análogo de la Java virtual machine con java

## Preparando el entorno para Linux

Una manera y creo la mejor opción para poder seleccionar entre diferentes versiones de Node es usar **nvm** _(Node Version Manager)

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
bash install_nvm.sh
source ~/.profile
```

Luego:

```
 nvm ls-remote
```

Listará todas las versiones disponibles de Node y finalmente ejecutar el comando `nvm install 12.9.1` e instalará Node en esa versión, así también podemos tener más versiones y cambiar entre ellas usando `nvm use [version]`
Para los que usan windows la descarga se hace desde esta página: [NVM for Windows](https://github.com/coreybutler/nvm-windows/releases)

```bash
sudo npm install -g @angular/cli
```

**Version** 

```bash
ng --version
```




## Crea tu primera aplicación en Angular

# 2. Estructuras de control


## Introducción al Angular CLI y proyecto del curso


## String interpolation


## Data binding en Angular


## Uso de ngIf


## Uso de ngFor add y delete


## Uso de ngFor para recorrer objetos


## Uso de ngSwitch

# 3. Componentes


## ¿Qué son los componentes y decoradores?


## Uso de Inputs y Outputs


## Ciclo de vida de los componentes



## Estilos para mostrar la lista de productos


## Uso de ng generate y ng lint

# 4. Pipes y Directivas


## Usando los pipes de Angular


## Construyendo un propio pipe


## Construyendo una directiva propia

# 5. Módulos y Rutas


## Introducción al NgModule


## Creando rutas en Angular


## Creando la página home de la tienda


## Usando routerLink y routerActive


## Creando el detalle de cada producto


## Elaboración de la página de detalle de producto


## Creando el módulo del website con vistas anidadas


## Preparar el proyecto para Lazy Loading


## Implementación del Lazy Loading


## Creando un shared module y core module


## Guardianes

# 6. UI/UX, estilos


## alando Angular Material
## 

## Instalando un sistema de grillas


## Creando el header


## Estilos a product-card


## Creando vistas con Angular schematic

# 7.Servicios

## Creando nuestros propios servicios: HTTP Client


## Haciendo una solicitud GET desde el servicio


## Haciendo una solicitud POST desde el servicio


## Haciendo una solicitud PUT y DELETE desde el servicio


## Ambientes en Angular


## Lista de inventario y detalle


## Cierre de ejercicio de detalle

# 8. Formularios


## Introducción al FormControl


## Creando el formulario de productos


## Ajustar estilos en un formulario


## Validaciones personalizadas


## Editar un producto a través de un formulario

# 9. Programación reactiva


## Añadiendo productos al carrito


## Usa un pipe para hacer un contador de productos


## Creando la página de la orden y uso de async

# 10. Firebase


## Instalar Angular Firebase y configurar Firebase Auth


## Implementando Auth y Guards


## Subiendo una imagen a Firebase Storage


## Haciendo deploy a Firebase Hosting

# 11. Conclusiones


## Repaso del curso